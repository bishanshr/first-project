from django.contrib import admin
from .models import Service, Post, Page, Slider, Contact


# Register your models here.
admin.site.register(Slider)
admin.site.register(Post)
admin.site.register(Page)
admin.site.register(Service)
admin.site.register(Contact)