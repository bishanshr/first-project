from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('about', views.about, name='about'),
    path('blog', views.blog, name='blog'),
    path('contact', views.contact, name='contact'),
    path('service', views.ServiceView.as_view(), name='service'),
    path('test', views.TestView.as_view(), name='test'),
]
