from django.db import models
from django.template.defaultfilters import slugify
from django import forms
from ckeditor.fields import RichTextField

class Slider(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='image/')


class Page(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=40)
    content = RichTextField()

    def slug(self):
        return slugify(self.title)

    def __str__(self):
        return self.title


class Service(models.Model):
    image = models.ImageField(upload_to='image/')
    title = models.CharField(max_length=200)
    content = RichTextField()


class Post(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=40)
    content = models.TextField()
    image = models.ImageField(upload_to='image/')

    def slug(self):
        return slugify(self.title)


class Contact(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    message = models.TextField()

    def __str__(self):
        return self.email


