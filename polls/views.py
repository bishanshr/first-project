from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponse
from .models import Page
from django.template import loader
from .models import Page, Service,Contact
from django.views import generic



class IndexView(generic.ListView):
    model = Service
    template_name = 'polls/index.html'
    # context_object_name = 'page'


def about(request):
    template = loader.get_template('polls/about.html')
    return HttpResponse( template.render({}, request))


def blog(request):
    template = loader.get_template('polls/blog.html')
    return HttpResponse( template.render({}, request))

def contact(request):

    if request.method =="POST":
        Name = request.POST['Name']
        Email = request.POST['Email']
        text = request.POST['text']
        print (Name, Email, text)

        ins = Contact(name=Name, email=Email, message=text)
        ins.save()

        template = loader.get_template('polls/index.html')
        return HttpResponse(template.render({}, request))

    else:
        template = loader.get_template('polls/contact.html')
        return HttpResponse(template.render({}, request))

    return HttpResponse(template.render({}, request))



class ServiceView(generic.ListView):
    model = Service
    template_name = 'polls/service.html'


class TestView(generic.ListView):
    model = Service
    template_name = 'polls/test.html'

